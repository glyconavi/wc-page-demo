import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import 'wc-page-menu/wc-page-menu.js';
import 'wc-page-limit/pageOffsetChooser.js';
import 'wc-chart/chart.js';
import 'wc-page-table/pageTable.js';

class PageDemo extends PolymerElement {
  static get template() {
    return html`
<style type="text/css">

</style>
 
<iron-ajax auto="" url="https://test.sparqlist.glyconavi.org/api/GlycoSampleList_v2_page?offset={{_calculateOffset(page)}}&limit={{limit}}" handle-as="json" last-response="{{resultdata}}"></iron-ajax>
<iron-ajax auto="" url="https://test.sparqlist.glyconavi.org/api/GlycoSample_Disease_List_chart" handle-as="json" last-response="{{diseaseresultdata}}"></iron-ajax>
<wc-page-menu page="{{page}}" limit="{{limit}}"></wc-page-menu>
<wc-page-offset limit="{{limit}}"></wc-page-offset>
<wc-page-table page="{{page}}" limit="{{limit}}"></wc-page-table>
<wc-chart selection="{{selection}}"></wc-chart>
`;
  }
  static get properties() {
    return {
      selection: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      resultdata: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      diseaseresultdata: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      page: {
        type: String,
        notify: true,
        value: "1"
      },
      limit: {
        type: String,
        notify: true,
        value: "10"
      },
      offset: {
        type: String,
        notify: true,
        value: "0"
      },
      filter: {
        type: String,
        notify: true,
      }
    };
  }
  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
  _formatLimit(value) {
    console.log("formatLimit: " + value);
    var choice = "10";
    switch (value) {
      case 0:
        choice = "10";
        break;
      case 1:
        choice = "50";
        break;
      case 2:
        choice = "100";
        break;
    }
    return choice;
  }
}

customElements.define('wc-pagedemo', PageDemo);
